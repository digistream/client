<?php

declare(strict_types=1);

namespace DigiStream\Monolog;

use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class NotifyHandler extends AbstractProcessingHandler
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $digiStreamDSN;
    /**
     * @var string
     */
    private $digiStreamApiToken;
    /**
     * @var HttpClient
     */
    private $httpClient;
    /**
     * @var bool
     */
    private $userAgentFromHostname;
    private $strictMode = false;

    /**
     * Constructor.
     *
     * @param HttpClientInterface $httpClient
     * @param LoggerInterface $logger
     * @param string $digiStreamDSN
     * @param int|string $level The minimum logging level at which this
     *                             handler will be triggered
     * @param bool $bubble Whether the messages that are handled can
     *                             bubble up the stack or not
     * @param bool $userAgentFromHostname
     */
    public function __construct(
        HttpClientInterface $httpClient,
        LoggerInterface $logger,
        string $digiStreamDSN,
        $level = Logger::DEBUG,
        bool $bubble = true,
        $userAgentFromHostname = false
    ) {
        parent::__construct($level, $bubble);
        $this->httpClient = $httpClient;
        $this->logger = $logger;

        $parts = parse_url($digiStreamDSN);
        $this->digiStreamDSN = "{$parts['scheme']}://{$parts['host']}/api{$parts['path']}/store/";
        $this->digiStreamApiToken = $parts['user'];
        $this->userAgentFromHostname = $userAgentFromHostname;
    }

    public function isHandling(array $record): bool
    {
        if ($this->strictMode) {
            return $record['level'] == $this->getLevel();
        } else {
            return $record['level'] >= $this->getLevel();
        }
    }

    public function setHandlingLogLevelStrictMode(bool $strictMode)
    {
        $this->strictMode = $strictMode;
    }

    /**
     * {@inheritdoc}
     * @param array $record
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function write(array $record): void
    {
        $record['level'] = strtolower($record['level_name']);
        try {

            $options = [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Authorization' => 'Bearer ' . $this->digiStreamApiToken
                ],
                'json'    => $record
            ];

            if ($this->userAgentFromHostname) {
                $options['headers']['User-Agent'] = '[DigiStream]-' . gethostname();
            }

            $response = $this->httpClient->request('POST', $this->digiStreamDSN, $options);

            if ($response->getStatusCode() !== 200) {
                $this->logger->info(
                    sprintf('Failed to send the event to DigiStream. Reason: "%s".', $response->getContent()),
                    [
                        'exception' => $response->getStatusCode() . ' ' . $response->getContent(),
                        'event'     => $record,
                    ]
                );
            }

        } catch (\Exception $exception) {

            $this->logger->critical(
                sprintf('Failed to send the event to DigiStream. Reason: "%s".', $exception->getMessage()),
                [
                    'exception' => $exception,
                    'event'     => $record,
                ]
            );

        }
    }
}
