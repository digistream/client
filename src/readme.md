###### monolog config

```yaml
monolog:
   channels: ['client', 'admin']
   handlers:
     notify:
       type: service
       id: DigiStreamClient\Monolog\NotifyHandler
       level: debug
       channels: ['client', 'admin']
```


###### services.yaml
```yaml
services:
  _defaults:
    bind:
      Psr\Log\LoggerInterface $businessLogger: '@monolog.logger.client'
      Psr\Log\LoggerInterface $adminLogger: '@monolog.logger.admin'
      $digiStreamDSN: '%env(DIGISTREAM_DSN)%'
  DigiStream\Monolog\NotifyHandler:
    class: DigiStream\Monolog\NotifyHandler
    calls: #optional, default debug
      - [setLevel, ['critical']] 
```



###### .env
```.dotenv
###> sentry/sentry-symfony ###
SENTRY_DSN=https://{API_TOKEN_WITH_WRITE_PERMISSIONS}@digistream.tech/{STREAM_ID}
###< sentry/sentry-symfony ###

###> digiStream ###
DIGISTREAM_DSN=https://digistream.tech/
DIGISTREAM_TOKEN={API_TOKEN_WITH_WRITE_PERMISSIONS}
###< digiStream ###
```


