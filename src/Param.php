<?php
declare(strict_types=1);

namespace DigiStream;

class Param
{
    const SUB_STREAM_ID = 'subStreamId';

    const EXTRA = 'extra';
    const LINKS = 'links';
    const LINKS_URL = 'url';
    const LINKS_LABEL = 'label';

    const TITLE = 'title';
    const DESCRIPTION = 'description';

    /** https://www.php.net/manual/en/dateinterval.construct.php */
    const NOTIFICATION_INTERVAL = 'notificationInterval';
    const NOTIFICATION_INTERVAL_5_MIN = 'PT5M';
    const NOTIFICATION_INTERVAL_HOUR = 'PT1H';
    const NOTIFICATION_INTERVAL_DAY = 'P1D';
}
